package co.barakastudio.myspeechrecognizer;

import android.os.Bundle;
import android.os.RemoteException;
import android.speech.RecognitionListener;
import android.speech.RecognitionService;
import android.speech.RecognitionService.Callback;

/**
 * Created by Nikola on 10/22/2017.
 */

public class VoiceResultsListener  implements RecognitionListener {

    private Callback m_UserSpecifiedListener;

    /**
     *
     * @param userSpecifiedListener
     */
    public VoiceResultsListener(Callback userSpecifiedListener) {
        m_UserSpecifiedListener = userSpecifiedListener;
    }

    @Override
    public void onBeginningOfSpeech() {
        try {
            m_UserSpecifiedListener.beginningOfSpeech();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        try {
            m_UserSpecifiedListener.bufferReceived(buffer);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEndOfSpeech() {
        try {
            m_UserSpecifiedListener.endOfSpeech();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int error) {
        try {
            m_UserSpecifiedListener.error(error);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEvent(int eventType, Bundle params) { ; }

    @Override
    public void onPartialResults(Bundle partialResults) {
        try {
            m_UserSpecifiedListener.partialResults(partialResults);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        try {
            m_UserSpecifiedListener.readyForSpeech(params);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResults(Bundle results) {
        try {
            m_UserSpecifiedListener.results(results);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        try {
            m_UserSpecifiedListener.rmsChanged(rmsdB);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
