package co.barakastudio.myspeechrecognizer;

import android.content.Intent;
import android.speech.RecognitionService;
import android.speech.SpeechRecognizer;
import android.util.Log;

/**
 * Created by Nikola on 10/22/2017.
 */

public class SpeechService extends RecognitionService {

    private SpeechRecognizer m_EngineSR;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("SimpleVoiceService", "Service started");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("SimpleVoiceService", "Service stopped");
    }

    @Override
    protected void onCancel(Callback listener) {
        m_EngineSR.cancel();
    }

    @Override
    protected void onStartListening(Intent recognizerIntent, Callback listener) {
        m_EngineSR.setRecognitionListener(new VoiceResultsListener(listener));
        m_EngineSR.startListening(recognizerIntent);
    }

    @Override
    protected void onStopListening(Callback listener) {
        m_EngineSR.stopListening();
    }
}


/**
 *
 */