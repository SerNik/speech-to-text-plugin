package co.barakastudio.speechrecognitionplugin;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognitionService;
import android.speech.RecognitionService.Callback;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import com.unity3d.player.UnityPlayer;
import java.util.ArrayList;

import static android.speech.SpeechRecognizer.ERROR_AUDIO;
import static android.speech.SpeechRecognizer.ERROR_CLIENT;
import static android.speech.SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS;
import static android.speech.SpeechRecognizer.ERROR_NETWORK;
import static android.speech.SpeechRecognizer.ERROR_NETWORK_TIMEOUT;
import static android.speech.SpeechRecognizer.ERROR_NO_MATCH;
import static android.speech.SpeechRecognizer.ERROR_RECOGNIZER_BUSY;
import static android.speech.SpeechRecognizer.ERROR_SERVER;
import static android.speech.SpeechRecognizer.ERROR_SPEECH_TIMEOUT;
import static android.speech.SpeechRecognizer.createSpeechRecognizer;

public class SpeechService extends RecognitionService implements RecognitionListener {

    public static SpeechRecognizer m_EngineSR;
    public static Context mContext;
    public static SpeechService mSpeechService;
    //public static android.app.Activity mActivity;
    static String TAG = "VOICE RECOGNITION";
    static String GameObjectName;

    public static void SetGameObjectName(String name) {
        GameObjectName = name;
    }
    public static void StartListenning() {
        Log.i(TAG, "START THE SERVICE! ");

        if( UnityPlayer.currentActivity != null) {

            UnityPlayer.UnitySendMessage(GameObjectName, "GetMessage", "TRY 2 RESTART SERVICE");
            Intent intent = new Intent(UnityPlayer.currentActivity, SpeechService.class);
            UnityPlayer.currentActivity.startService(intent);
            // }
        }

    }

    //The service is created and voice recognition service has been started
    @Override
    public void onCreate() {

        Log.i(TAG, "onCreate()");
        if(mSpeechService==null ) {
            mContext = getApplicationContext();
            mSpeechService = this;
            m_EngineSR = createSpeechRecognizer(this);
            m_EngineSR.setRecognitionListener(this);
            Intent voiceIntent = RecognizerIntent.getVoiceDetailsIntent(mContext);
            m_EngineSR.startListening(voiceIntent);

        }
        super.onCreate();
    }

    private void checkForCommands(Bundle bundle) {
        ArrayList<String> voiceText = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        if (voiceText != null) {
            if (voiceText.size() > 0) {
                SendMessageToUnity(voiceText.get(0));
            } else {
                SendMessageToUnity("nothing!");
            }

        } else {
            SendMessageToUnity("voiceText empty!");
        }
    }

    /**
     *  Triggered by the activity.startService(intent);
     * @param intent intent send
     * @param flags flags
     * @param startId startid
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        SendMessageToUnity("onStartCommand()!");
        if(mSpeechService!=null ) {
            mContext = getApplicationContext();
            restartListenning(mContext);
        }

        return Service.START_NOT_STICKY;
    }

    public static void restartListenning(Context context_){

        if(mSpeechService!=null) {
            mContext = context_;
            if (m_EngineSR == null){
                m_EngineSR = createSpeechRecognizer(mSpeechService);
                m_EngineSR.setRecognitionListener(mSpeechService);
            }else{
                m_EngineSR.stopListening();
            }
            Intent voiceIntent = RecognizerIntent.getVoiceDetailsIntent(mContext);
            m_EngineSR.startListening(voiceIntent);
        }
    }


    /**
     * Send the data to Unity
     * @param text text  string
     */
    public void SendMessageToUnity(String text){
        if(m_EngineSR!=null) {

            if(text != null && text.isEmpty() )Log.i("TESTING: ", "final message! =" + text);
            try {

                if( UnityPlayer.currentActivity != null){
                    UnityPlayer.UnitySendMessage(GameObjectName, "GetMessage", text);
                    SendStopWritingUnity();
                }

            } catch (Exception e) {
                // Log.e(TAG, "UnitySendMessage failed" + e.getMessage());
            }
            // m_EngineSR.stopListening();
        }
    }

    /**
     * Send an error to Unity
     * @param text errors string
     */
    public void SendErrorToUnity(String text){
        if(m_EngineSR!=null) {
            try {

                if( UnityPlayer.currentActivity != null) UnityPlayer.UnitySendMessage(GameObjectName, "GetError", text);

            } catch (Exception e) {
                // Log.e(TAG, "UnitySendMessage failed" + e.getMessage());
            }
            // m_EngineSR.stopListening();
        }
    }

    /**
     * We notifiy to unity than the player as stopped writing (link to the stop listenning funciton)
     */
    public void SendStopWritingUnity(){
        if(m_EngineSR!=null) {
            try {

                if( UnityPlayer.currentActivity != null){
                    UnityPlayer.UnitySendMessage(GameObjectName, "StopWriting","");

                }

            } catch (Exception e) {
                // Log.e(TAG, "UnitySendMessage failed" + e.getMessage());
            }
            // m_EngineSR.stopListening();
        }
    }


    /***
     * SERVICE STUFF
     */


    @Override
    public void onDestroy() {
        if(m_EngineSR!= null)m_EngineSR.cancel();
        Log.i("SimpleVoiceService", "Service stopped");
        super.onDestroy();
    }



    @Override
    protected void onStartListening(Intent recognizerIntent, Callback listener) {

    }

    @Override
    protected void onCancel(Callback listener) {

    }

    @Override
    protected void onStopListening(Callback listener) {
        SendStopWritingUnity();
    }


    @Override
    public void onReadyForSpeech(Bundle params) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float rmsdB){
    }

    @Override
    public void onBufferReceived(byte[] buffer) {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int error) {
        try {


            String message;
            switch (error)
            {
                case ERROR_AUDIO:
                    message = "Audio recording error";
                    break;
                case ERROR_CLIENT:
                    message = "Client side error";
                    break;
                case ERROR_INSUFFICIENT_PERMISSIONS:
                    message = "Insufficient permissions";
                    break;
                case ERROR_NETWORK:
                    message = "Network error";
                    break;
                case ERROR_NETWORK_TIMEOUT:
                    message = "Network timeout";
                    break;
                case ERROR_NO_MATCH:
                    message = "No match";
                    break;
                case ERROR_RECOGNIZER_BUSY:
                    message = "RecognitionService busy";
                    break;
                case ERROR_SERVER:
                    message = "error from server";
                    break;
                case ERROR_SPEECH_TIMEOUT:
                    message = "No speech input";
                    break;
                default:
                    message = "Didn't understand, please try again.";
                    break;
            }

            SendErrorToUnity(message);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //m_SRListener.onError(error);
    }

    @Override
    public void onResults(Bundle results) {
        checkForCommands(results);
    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        //checkForCommands(partialResults);
    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }


}